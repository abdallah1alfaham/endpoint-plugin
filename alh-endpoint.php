<?php

/**
 * @package ALH EndpointPlugin
 */

/*
Plugin Name: ALH Endpoint Plugin
Plugin URI: http://alh.com
Description: A custom WP plugin when a visitor navigates to its endpoint, the plugin has to send an HTTP request to a REST API
endpoint. The API is available at https://jsonplaceholder.typicode.com and the endpoint to call is
/users.
Version: 1.0.0
Author: ALH Software
Author URI: http://alh.com
License: MIT
Text Domain: ALH Endpoint Plugin

*/

/*
MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


if (!function_exists('add_action')) {
    echo 'Access Denied!!';
    exit;
}

class ALHEndpointPlugin
{

    function my_plugin_template_redirect_intercept()
    {
        global $wp_query;
        if ($wp_query->get('myusers')) {

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://jsonplaceholder.typicode.com/users",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Accept: application/json"
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $users = json_decode($response, true);

            include(plugin_dir_path(__FILE__)."/allusers.php");

            exit;
        } else{
            echo "Something Wrong!";
            exit;
        }
    }

    function my_plugin_add_rewrite_rules()
    {
        $rule = '^/blog/example/some-var/([0-9]+)/?$';
        add_rewrite_rule(
            $rule,
            'index.php?myusers=1&some-var=$matches[1]',
            'top');
    }

    function my_plugin_rewrites_init()
    {
        add_rewrite_tag('%myusers%', '(^[0-9a-zA-z]+)');
        $this->my_plugin_add_rewrite_rules();
    }

    function activate()
    {
        global $wp_rewrite;
        $this->my_plugin_add_rewrite_rules();
        $wp_rewrite->flush_rules();
        flush_rewrite_rules();
    }

    function deactivate()
    {
        flush_rewrite_rules();
    }

}

if (class_exists('ALHEndpointPlugin')) {
    $alhplugin = new ALHEndpointPlugin();
}

register_activation_hook(__FILE__, [$alhplugin, 'activate']);

register_deactivation_hook(__FILE__, [$alhplugin, 'deactivate']);

add_action('template_redirect', [$alhplugin, 'my_plugin_template_redirect_intercept']);

add_action('init', [$alhplugin, 'my_plugin_rewrites_init']);

