<?php
if (!function_exists('add_action')) {
    echo 'Access Denied!!';
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Custom Endpoint</title>
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type='text/javascript'
            src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js'></script>
</head>
<body>
<div class='container'>
    <h2 class="h2 mt-4">Users Endpoint</h2>
    <hr/>
    <div class="card border-primary mb-3 mt-3" style="max-width: 70rem;">
        <div class="card-header"></div>
        <div class="card-body">
            <h4 class="card-title"></h4>
            <div class="card-content">

            </div>
        </div>
    </div>

    <table class='table table-hover table-striped mt-5'>
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Username</th>
            <th>E-mail</th>
            <th>Phone</th>
            <th>Website</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user): ?>
            <tr>
                <td><a class="id" href='#'><?= $user['id']; ?></a></td>
                <td><a class="name" href='#'><?= $user['name']; ?></a></td>
                <td><a class="username" href='#'><?= $user['username']; ?></a></td>
                <td><?= $user['email']; ?></td>
                <td><?= $user['phone']; ?></td>
                <td><a target="_blank" href='http://<?= $user['website']; ?>'>Visit</a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script>
    $('.card').hide();
    $(document).ready(function () {
        $("a").click(function () {
            var myClass = $(this).attr("class");
            var value = $(this).html();

            if (myClass === 'id' || myClass === 'name' || myClass === 'username') {
                var url = 'https://jsonplaceholder.typicode.com/users?' + myClass + '=' + value;
                var settings = {
                    "url": url,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "Accept": "application/json"
                    },
                };

                $.ajax(settings).done(function (response) {
                    if (response.length > 0) {
                        $('.card-content').html('');
                        $('.card').show();
                        $('.card-header').html("User ID:" + response[0].id);
                        $('.card-title').html("Name: " + response[0].name);
                        var info = '';

                        for (var key in response[0]) {
                            if (response[0].hasOwnProperty(key) && key !== 'id' && key !== 'name') {
                                if (key === 'address') {
                                    var addr = "<h5 class='card-text'>" + key + ": ";
                                    for (var key1 in response[0]['address']) {
                                        if (key1 === 'geo') {
                                            var geo = "geo: [";
                                            for (var key2 in response[0]['address']['geo']) {
                                                if (response[0]['address']['geo'].hasOwnProperty(key2)) {
                                                    if (response[0]['address']['geo'][key2] != Object.values(response[0]['address']['geo']).pop())
                                                        geo += key2 + ": " + response[0]['address']['geo'][key2] + ", ";
                                                    else
                                                        geo += key2 + ": " + response[0]['address']['geo'][key2];
                                                }
                                            }
                                            geo += "]";
                                            addr += geo;
                                        } else {
                                            if (response[0][key].hasOwnProperty(key1)) {
                                                if (response[0][key][key1] != Object.values(response[0][key][key1]).pop())
                                                    addr += key1 + ": " + response[0][key][key1] + ", ";
                                                else
                                                    addr += key1 + ": " + response[0][key][key1];
                                            }
                                        }
                                    }
                                    addr += "</h5>";
                                    info += addr;
                                } else if (key === 'company') {
                                    var comp = "<h5 class='card-text'>" + key + ": ";
                                    for (var key1 in response[0]['company']) {
                                        if (response[0]['company'][key1] != Object.values(response[0]['company']).pop())
                                            comp += key1 + ": " + response[0]['company'][key1] + ", ";
                                        else
                                            comp += key1 + ": " + response[0]['company'][key1];
                                    }
                                    comp += "</h5>";
                                    info += comp;
                                } else if (key === 'website') {
                                    info += "<h5 class='card-text'>" + key + ": <a target='_blank' href='http://" + response[0][key] + "'>" + response[0][key] + "</a></h5>";
                                } else {
                                    info += "<h5 class='card-text'>" + key + ": " + response[0][key] + "</h5>";
                                }
                            }
                        }
                        $('.card-content').append(info);
                    }
                });
            }
        });
    });
</script>
</body>
</html>