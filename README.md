# ALH Endpoint Plugin
A simple Task Management System

## About
Description: A custom WP plugin when a visitor navigates to its endpoint, the plugin has to send an HTTP request to a REST API endpoint.

## Installation

Edit your ```composer.json``` file and add the following line into ```"require": {...}```:

```
"alh-software/endpoint-plugin": "1.0.0"
```

And then run:

```bash
composer install
```

OR, you can just run the following command, since our plugin is available via "Packagist" The PHP Package Repository:

```bash
composer require alh-software/endpoint-plugin
```

Plugin should be installed in ```wp-content/plugins/``` directory.

## Activation
Go to WP Admin Dashboard > Plugins and click ```Activate``` link under the new plugin.

## Run the requests
To run the plugin functionality you should call the url:

```url
example.com?myusers=yes
```

Where ```example.com``` is your WP Website with the plugin installed.


## Run the tests 

You can run the project tests using:

```bash
cd DIR/wp-content/plugins/alh-endpoint
```

Where ```DIR``` is your WP Website directory.

Then run:

```bash
vendor/bin/phpunit
```

---